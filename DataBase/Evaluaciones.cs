﻿using System;
using System.Collections.Generic;

namespace AccionDeMejora.DataBase
{
    public partial class Evaluaciones
    {
        public Evaluaciones()
        {
            Calificaciones = new HashSet<Calificaciones>();
        }

        public int Id { get; set; }
        public int ExposicionId { get; set; }
        public string EvaluadorId { get; set; }
        public int? PonenteId { get; set; }

        public virtual Exposiciones Exposicion { get; set; }
        public virtual Ponentes Ponente { get; set; }
        public virtual ICollection<Calificaciones> Calificaciones { get; set; }
    }
}
