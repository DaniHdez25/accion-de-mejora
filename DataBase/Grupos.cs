﻿using System;
using System.Collections.Generic;

namespace AccionDeMejora.DataBase
{
    public partial class Grupos
    {
        public Grupos()
        {
            Ponentes = new HashSet<Ponentes>();
        }

        public int Id { get; set; }
        public string Nombre { get; set; }
        public string Descripcion { get; set; }

        public virtual ICollection<Ponentes> Ponentes { get; set; }
    }
}
