﻿using System;
using System.Collections.Generic;

namespace AccionDeMejora.DataBase
{
    public partial class Exposiciones
    {
        public Exposiciones()
        {
            Evaluaciones = new HashSet<Evaluaciones>();
        }

        public int Id { get; set; }
        public string Titulo { get; set; }
        public string Descripcion { get; set; }
        public int EvaluadorId { get; set; }

        public virtual Evaluadores Evaluador { get; set; }
        public virtual ICollection<Evaluaciones> Evaluaciones { get; set; }
    }
}
