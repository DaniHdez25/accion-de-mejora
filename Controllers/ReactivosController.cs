﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using AccionDeMejora.DataBase;

namespace AccionDeMejora.Controllers
{
    public class ReactivosController : Controller
    {
        private readonly expositoryContext _context;

        public ReactivosController(expositoryContext context)
        {
            _context = context;
        }

        // GET: Reactivos
        public async Task<IActionResult> Index()
        {
            return View(await _context.Reactivos.ToListAsync());
        }

        // GET: Reactivos/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var reactivos = await _context.Reactivos
                .FirstOrDefaultAsync(m => m.Id == id);
            if (reactivos == null)
            {
                return NotFound();
            }

            return View(reactivos);
        }

        // GET: Reactivos/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Reactivos/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Titulo,Descripcion")] Reactivos reactivos)
        {
            if (ModelState.IsValid)
            {
                _context.Add(reactivos);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(reactivos);
        }

        // GET: Reactivos/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var reactivos = await _context.Reactivos.FindAsync(id);
            if (reactivos == null)
            {
                return NotFound();
            }
            return View(reactivos);
        }

        // POST: Reactivos/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Titulo,Descripcion")] Reactivos reactivos)
        {
            if (id != reactivos.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(reactivos);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ReactivosExists(reactivos.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(reactivos);
        }

        // GET: Reactivos/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var reactivos = await _context.Reactivos
                .FirstOrDefaultAsync(m => m.Id == id);
            if (reactivos == null)
            {
                return NotFound();
            }

            return View(reactivos);
        }

        // POST: Reactivos/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var reactivos = await _context.Reactivos.FindAsync(id);
            _context.Reactivos.Remove(reactivos);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool ReactivosExists(int id)
        {
            return _context.Reactivos.Any(e => e.Id == id);
        }
    }
}
