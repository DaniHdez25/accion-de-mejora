﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using AccionDeMejora.DataBase;

namespace AccionDeMejora.Controllers
{
    public class ExposicionesController : Controller
    {
        private readonly expositoryContext _context;

        public ExposicionesController(expositoryContext context)
        {
            _context = context;
        }

        // GET: Exposiciones
        public async Task<IActionResult> Index()
        {
            var expositoryContext = _context.Exposiciones.Include(e => e.Evaluador);
            return View(await expositoryContext.ToListAsync());
        }

        // GET: Exposiciones/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var exposiciones = await _context.Exposiciones
                .Include(e => e.Evaluador)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (exposiciones == null)
            {
                return NotFound();
            }

            return View(exposiciones);
        }

        // GET: Exposiciones/Create
        public IActionResult Create()
        {
            ViewData["EvaluadorId"] = new SelectList(_context.Evaluadores, "Id", "Id");
            return View();
        }

        // POST: Exposiciones/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Titulo,Descripcion,EvaluadorId")] Exposiciones exposiciones)
        {
            if (ModelState.IsValid)
            {
                _context.Add(exposiciones);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["EvaluadorId"] = new SelectList(_context.Evaluadores, "Id", "Id", exposiciones.EvaluadorId);
            return View(exposiciones);
        }

        // GET: Exposiciones/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var exposiciones = await _context.Exposiciones.FindAsync(id);
            if (exposiciones == null)
            {
                return NotFound();
            }
            ViewData["EvaluadorId"] = new SelectList(_context.Evaluadores, "Id", "Id", exposiciones.EvaluadorId);
            return View(exposiciones);
        }

        // POST: Exposiciones/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Titulo,Descripcion,EvaluadorId")] Exposiciones exposiciones)
        {
            if (id != exposiciones.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(exposiciones);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ExposicionesExists(exposiciones.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["EvaluadorId"] = new SelectList(_context.Evaluadores, "Id", "Id", exposiciones.EvaluadorId);
            return View(exposiciones);
        }

        // GET: Exposiciones/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var exposiciones = await _context.Exposiciones
                .Include(e => e.Evaluador)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (exposiciones == null)
            {
                return NotFound();
            }

            return View(exposiciones);
        }

        // POST: Exposiciones/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var exposiciones = await _context.Exposiciones.FindAsync(id);
            _context.Exposiciones.Remove(exposiciones);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool ExposicionesExists(int id)
        {
            return _context.Exposiciones.Any(e => e.Id == id);
        }
    }
}
